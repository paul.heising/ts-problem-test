export class MyTest {
  private _foo = 0;

  private get foo() {
    return this._foo;
  }

  private set foo(value: number) {
    this._foo = value;
    // Do something special with foo...
  }

  public someFunction() {
    // This fails:
    this.foo += 10;

    // But it's the same as this, which works:
    // this.foo = this.foo + 10;
  }
}
